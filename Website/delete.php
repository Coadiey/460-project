<?php
include("common.php");
ob_start();
?>
	<div>
	<table id="table">			
		<tr>
			<th>Rows to Delete</th>
			<th>Vin</th>
			<th>Make</th>
			<th>Model</th>
			<th>Year</th>
			<th>Damage Type</th>
			<th>Damage Area</th>
			<th>Picture</th>
			<th>Notes</th>
		</tr>
		<form method="POST" action="delete.php" multiple="multiple"> 
			<input type='submit' name='submit' value='submit'>
<?php  
  $i = 0;
  foreach ($sqldata as $row){
      $i++;
  ?> 		
	        <tr>
	        	<td><input type="checkbox" value="<?php echo htmlspecialchars($row['vin']); ?>" name="vin[]"></td>
	            <td><?php echo htmlspecialchars($row['vin']); ?></td>            	
	            <td><?php echo htmlspecialchars($row['make']); ?></td>
	            <td><?php echo htmlspecialchars($row['model']); ?></td>
	            <td><?php echo htmlspecialchars($row['year']); ?></td>
	            <td><?php echo htmlspecialchars($row['damagetype']); ?></td>
	            <td><?php echo htmlspecialchars($row['damagearea']); ?></td>
	            <td class="imageContainer"><img src="images\<?php echo htmlspecialchars($row['pictures']);?>" alt="<?php echo htmlspecialchars($row['pictures']);?>" height="100" width="100"></td>
	            <td><?php echo htmlspecialchars($row['notes']); ?></td>
	        </tr> 	       	
<?php
}
?>
	</table>
	</form> 
	</div>
</body>
</html>

<?php
// Check if form is submitted successfully 
if(!empty($_POST["vin"]))  
{ 
    // Check if any option is selected 
    if(isset($_POST["vin"]))  
    {     	
    	$i = 0;
        // Retrieving each selected option 
        foreach ($_POST['vin'] as $vin)  {			
			$stmt = $conn->prepare("DELETE FROM cars WHERE vin = ?");
			$stmt->bind_param("s", $vin);
    		$stmt->execute();
    		$i++;
    		if($stmt->affected_rows === 0)exit('No rows updated');
    		$stmt->close(); 
			echo $conn->info;	      		 
		} 
		// Should output 1
	    echo "Deleted $i rows.";
	   $conn->close();
	    header("Location: http://localhost/website/delete.php");
  		exit;
	}	
else{
    echo "Select an option first."; 
}
}  

?>