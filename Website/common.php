<?php
	$conn = new mysqli("localhost", "root", "", "scrapyard") or 
	die("Connection failed: %s/n". $conn -> error);
$sqlquery = "SELECT * FROM cars NATURAL JOIN carmodels;";
$sqldata = $conn->query($sqlquery);
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Scrapyard Home</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="tablestyle.css">
	<link rel="icon" 
      type="image/png" 
      href="background_image/scrapyardIcon.png">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="scrapyard.js"></script>
	<div class="topnav" id="topnav">
		<a href="home.php">Home</a>
		<a href="insert.php">Insert</a>
		<a href="update.php">Update</a>
		<a href="delete.php">Delete</a>
	</div>
</head>
<body>
		<div id="bg">
		<img src="background_image/background.jpg" alt="">
	</div>	
	<div id="main">