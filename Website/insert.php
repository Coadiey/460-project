<?php
include("common.php");
$sqlquery = "SELECT DISTINCT make FROM carmodels;";
$sqldata = $conn->query($sqlquery);
$sqliconn = new mysqli("localhost", "root", "", "scrapyard");
if ($sqliconn->connect_error){
  die("Connection failed: " . $sqliconn->connect_error);
}
$newstmt = $sqliconn->prepare("INSERT INTO cars (vin, model, year, damagetype, damagearea, pictures, notes) VALUES (?, ?, ?, ?, ?, ?, ?)");
$newstmt->bind_param("ssissss", $vin, $model, $year, $damagetype, $damagearea, $name, $notes);
?>


    <form method="POST" action="insert.php" enctype="multipart/form-data" id="insertinfo"> 
      <label>Vin #:
        <input type="text" name="vin" placeholder="Vin #"><br>
      <label>Make:
        <select name="make" id="make">
          <option hidden="">-- select make --</option>
        <?php  
          $i = 0;
          foreach ($sqldata as $row){
              $i++;
          ?>
              <option value= "<?= $row['make']; ?>"><?php echo $row['make'];?></option>
          <?php
          }
          ?>
            </select><br>
      <label>Model:
        <select name="model" id="model">              
              <option value="">-- select model -- </option>      
            </select><br><br>
      <label>Year #:
        <input type="text" name="year" placeholder="Year"><br>
      <label>Damage Type:
        <input type="text" name="damagetype" placeholder="Damage Type"><br>
      <label>Damage Area:
        <input type="text" name="damagearea" placeholder="Damage Area"><br>
      <label>Image: 
        <input type="file" name="image"><br>
      <label>Notes: 
        <input type="textarea" name="notes" placeholder="Notes"><br>      
      <input type="submit" value="Submit">
    </form>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $vin = htmlspecialchars($_POST["vin"]);
  $make = htmlspecialchars($_POST["make"]);
  $model = htmlspecialchars($_POST["model"]);
  $year = htmlspecialchars($_POST["year"]);
  $damagetype = htmlspecialchars($_POST["damagetype"]);
  $damagearea = htmlspecialchars($_POST["damagearea"]);
  $notes = htmlspecialchars($_POST["notes"]); 

  //For image uploading
   $upload_dir = "images";
   if ($_FILES["image"]["error"] == UPLOAD_ERR_OK) {
      $tmp_name = $_FILES["image"]["tmp_name"];
        
      // Ignore any path information in the filename
      $name = basename($_FILES["image"]["name"]);
        
      // Move the temp file and give it a new name 
      move_uploaded_file($tmp_name, "$upload_dir\\$name");
      echo "<p>Image uploaded successfully!</p>";
   }
   else {
      echo "<p>Error uploading the image.</p> <br> <br>";
   }

    //$sql = "INSERT INTO cars (vin, model, year, damagetype, damagearea, pictures, notes ) VALUES ('$vin', '$model', $year, '$damagetype', '$damagearea', '$name', '$notes');";
    //$sql = "INSERT INTO cars (vin, model, year, damagetype, damagearea, pictures, notes ) VALUES (?, ?, ?, ?, ?, ?, ?);";
    //$sql = "INSERT INTO cars (vin, model, year, damagetype, damagearea, notes, pictures ) VALUES ('?', '?', ?, '?', '?', '?', '?');";

    /*$stmt = $conn->prepare($sql);
    $vin = "'.$vin.";
    $model = "'.$model.";
    $damagetype = "'.$damagetype.";
    $damagearea = "'.$damagearea.";
    $name = "'.$name.";
    $notes = "'.$notes.";*/

    //$stmt->bind_param("ssissss", $vin, $model, $year, $damagetype, $damagearea, $name, $notes);
    //$stmt->execute();
    // Execute the command and output any error messages
    //if (!$conn->query($sql)) {
    //   die("Error ($conn->errno) $conn->error<br>SQL = $sql\n");
    //}

    // Should output 1
    //echo "Inserted $conn->affected_rows row.";
   // mysqli_close($conn);
    $newstmt->execute();
    $newstmt->close();
    $sqliconn->close();
}
?>  
    </div>
  </body>
</html>