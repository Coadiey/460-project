<?php
include("common.php");
?>
<div>
	<form action="update.php" method="POST">
	<input type="submit" name="submit" value="Submit Selection">
	<table id="table">			
		<tr>
			<th>Car to update</th>
			<th>Vin</th>
			<th>Make</th>
			<th>Model</th>
			<th>Year</th>
			<th>Damage Type</th>
			<th>Damage Area</th>
			<th>Picture</th>
			<th>Notes</th>
		</tr>		
<?php  
  $i = 0;
  foreach ($sqldata as $row){
      $i++;
  ?>
        <tr>
        	<td><input type="radio" name="car" value="<?php echo htmlspecialchars($row['vin']); ?>"></td>
            <td><?php echo htmlspecialchars($row['vin']); ?></td>
            <td><?php echo htmlspecialchars($row['make']); ?></td>
            <td><?php echo htmlspecialchars($row['model']); ?></td>
            <td><?php echo htmlspecialchars($row['year']); ?></td>
            <td><?php echo htmlspecialchars($row['damagetype']); ?></td>
            <td><?php echo htmlspecialchars($row['damagearea']); ?></td>
            <td class="imageContainer"><img src="images\<?php echo htmlspecialchars($row['pictures']);?>" alt="<?php echo htmlspecialchars($row['pictures']);?>" height="100" width="100"></td>
            <td><?php echo htmlspecialchars($row['notes']); ?></td>
        </tr>  
<?php
}
?>
	</table>
	</form>
</div>
<br><br>
<?php
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
     // The request is using the GET method
		$vin = htmlspecialchars($_POST['car']);

	    $con = mysqli_connect('localhost','root','','scrapyard');
	    if (!$con) {
	        die('Could not connect: ' . mysqli_error($con));
	    }

	    mysqli_select_db($con,"scrapyard");
	    $sql="SELECT * FROM cars NATURAL JOIN carmodels WHERE vin = '".$vin."'";
	    $result = mysqli_query($con,$sql);
	 
	    mysqli_close($con);
	    ?>
		<div>
			<table id="table">			
				<tr>
					<th>Vin</th>
					<th>Make</th>
					<th>Model</th>
					<th>Year</th>
					<th>Damage Type</th>
					<th>Damage Area</th>
					<th>Picture</th>
					<th>Notes</th>
				</tr>
		<?php  
		  $i = 0;
		  foreach ($result as $row){
		      $i++;
		      $vin = htmlspecialchars($row['vin']);
		      $oldMake = htmlspecialchars($row['make']);
		      $oldModel = htmlspecialchars($row['model']);
		      $oldYear = htmlspecialchars($row['year']);
		      $oldDamageType = htmlspecialchars($row['damagetype']);
		      $oldDamageArea = htmlspecialchars($row['damagearea']);
		      $oldNotes = htmlspecialchars($row['notes']);
		      $defaultImg = htmlspecialchars($row['pictures']);
		  ?>
		        <tr>
		            <td><?php echo $vin; ?></td>
		            <td><?php echo $oldMake; ?></td>
		            <td><?php echo $oldModel; ?></td>
		            <td><?php echo $oldYear; ?></td>
		            <td><?php echo $oldDamageType; ?></td>
		            <td><?php echo $oldDamageArea; ?></td>
		            <td class="imageContainer"><img src="images\<?php echo htmlspecialchars($row['pictures']);?>" alt="<?php echo htmlspecialchars($row['pictures']);?>" height="100" width="100"></td>
		            <td><?php echo $oldNotes; ?></td>
		        </tr> 
		        </table>
		        <form action="updateSQL.php" method="POST" enctype="multipart/form-data"> 
			        <input type="text" name="vin" placeholder="<?= $vin?>" value="<?= $vin?>" id="vinSelect" readonly><br>
			        <select name="make" id="make" disabled>
			        <?php  
			          $i = 0;
			          foreach ($sqldata as $row){
			              $i++;
			          ?>
			              <option value= "<?= $oldMake; ?>"><?php echo $oldMake;?></option>
			              <option value= "<?= $oldMake; ?>" hidden=""><?php echo $oldMake;?></option>
			          <?php
			          }
			          ?>
			            </select><br>
			        <select name="model" id="model" disabled>             
			        	  <option value="<?= $oldModel; ?>"><?= $oldModel; ?></option>   
			        	   <option value="<?= $oldModel; ?>" hidden=""><?= $oldModel; ?></option>     
			            </select><br><br>
			        <input type="text" name="year" value="<?= $oldYear; ?>" placeholder="<?= $oldYear; ?>"><br>
			        <input type="text" name="damagetype" value="<?= $oldDamageType; ?>" placeholder="<?= $oldDamageType; ?>"><br>
			        <input type="text" name="damagearea" value="<?= $oldDamageArea; ?>" placeholder="<?= $oldDamageArea; ?>"><br>
			        <label id="updateImage">If you want to replace the image do so now:  
			        <input type="file" name="image"><br>
			        <input type="textarea" name="notes" value="<?= $oldNotes; ?>" placeholder="<?= $oldNotes; ?>"><br>      
			        <input type="submit" value="Update!">			    
		<?php
		}
		?>	
			<input type="hidden" name="defaultImg" value="<?= $defaultImg?>">
			</form> 
		<?php
		}    
	    ?>
		</div>			

</body>
</html>