<?php
include("common.php");
?>

	<table id="table">			
		<tr>
			<th>Vin</th>
			<th>Make</th>
			<th>Model</th>
			<th>Year</th>
			<th>Damage Type</th>
			<th>Damage Area</th>
			<th>Picture</th>
			<th>Notes</th>
		</tr>
<?php  
  $i = 0;
  foreach ($sqldata as $row){
      $i++;
  ?>
        <tr>
            <td><?php echo htmlspecialchars($row['vin']); ?></td>
            <td><?php echo htmlspecialchars($row['make']); ?></td>
            <td><?php echo htmlspecialchars($row['model']); ?></td>
            <td><?php echo htmlspecialchars($row['year']); ?></td>
            <td><?php echo htmlspecialchars($row['damagetype']); ?></td>
            <td><?php echo htmlspecialchars($row['damagearea']); ?></td>
            <td class="imageContainer"><img src="images\<?php echo htmlspecialchars($row['pictures']);?>" alt="<?php echo htmlspecialchars($row['pictures']);?>" height="100" width="100"></td>
            <td><?php echo htmlspecialchars($row['notes']); ?></td>
        </tr>  
<?php
	}
?>
	</table>
</body>
</html>