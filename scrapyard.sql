-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2019 at 06:05 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scrapyard`
--
CREATE DATABASE IF NOT EXISTS `scrapyard` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `scrapyard`;

-- --------------------------------------------------------

--
-- Table structure for table `carmodels`
--

CREATE TABLE `carmodels` (
  `model` varchar(255) NOT NULL,
  `make` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `carmodels`
--

INSERT INTO `carmodels` (`model`, `make`) VALUES
('124', 'Abarth'),
('1300', 'Honda'),
('1400', 'Subaru'),
('147', 'Alfa Romeo'),
('156', 'Alfa Romeo'),
('159', 'Alfa Romeo'),
('1600', 'Subaru'),
('164', 'Alfa Romeo'),
('166', 'Alfa Romeo'),
('180', 'Mercedes-Benz'),
('190', 'Mercedes-Benz'),
('200', 'Mercedes-Benz'),
('200D', 'Mercedes-Benz'),
('220', 'Mercedes-Benz'),
('220D', 'Mercedes-Benz'),
('230', 'Mercedes-Benz'),
('240', 'Mercedes-Benz'),
('250', 'Mercedes-Benz'),
('260', 'Mercedes-Benz'),
('280', 'Mercedes-Benz'),
('300', 'Mercedes-Benz'),
('320', 'Mercedes-Benz'),
('33', 'Alfa Romeo'),
('350', 'Mercedes-Benz'),
('380', 'Mercedes-Benz'),
('400', 'Mercedes-Benz'),
('420', 'Mercedes-Benz'),
('450', 'Mercedes-Benz'),
('4C', 'Alfa Romeo'),
('500', 'Mercedes-Benz'),
('560', 'Mercedes-Benz'),
('595', 'Abarth'),
('595C', 'Abarth'),
('600', 'Mercedes-Benz'),
('695', 'Abarth'),
('695C', 'Abarth'),
('75', 'Alfa Romeo'),
('90', 'Alfa Romeo'),
('A-Class', 'Mercedes-Benz'),
('A110', 'Alpine'),
('A150', 'Mercedes-Benz'),
('Accent', 'Hyundai'),
('Accord', 'Honda'),
('ACTIVITY', 'Mercedes-Benz'),
('Acty', 'Honda'),
('Alfasud', 'Alfa Romeo'),
('Alfetta', 'Alfa Romeo'),
('AM', 'Aston Martin'),
('Armstrong Siddeley', 'Armstrong Siddeley'),
('Avenger', 'Dodge'),
('B-Class', 'Mercedes-Benz'),
('BERLINA', 'Alfa Romeo'),
('Brera', 'Alfa Romeo'),
('Brumby', 'Subaru'),
('BRZ', 'Subaru'),
('C-Class', 'Mercedes-Benz'),
('C220', 'Mercedes-Benz'),
('C230', 'Mercedes-Benz'),
('C280', 'Mercedes-Benz'),
('C320', 'Mercedes-Benz'),
('C350', 'Mercedes-Benz'),
('C43', 'Mercedes-Benz'),
('C63', 'Mercedes-Benz'),
('Caliber', 'Dodge'),
('Camry', 'Toyota'),
('Challenger', 'Dodge'),
('City', 'Honda'),
('Civic', 'Honda'),
('CL-Class', 'Mercedes-Benz'),
('CLA', 'Mercedes-Benz'),
('CLA-Class', 'Mercedes-Benz'),
('CLC-CLASS', 'Mercedes-Benz'),
('CLK-Class', 'Mercedes-Benz'),
('CLS-Class', 'Mercedes-Benz'),
('CLS53', 'Mercedes-Benz'),
('Concerto', 'Honda'),
('COUPE', 'Hyundai'),
('CR-V', 'Honda'),
('CR-X', 'Honda'),
('CR-Z', 'Honda'),
('CRUZ', 'Hyundai'),
('DB11', 'Aston Martin'),
('DB4', 'Aston Martin'),
('DB5', 'Aston Martin'),
('DB6', 'Aston Martin'),
('DB7', 'Aston Martin'),
('DB9', 'Aston Martin'),
('DBS', 'Aston Martin'),
('DBX', 'Aston Martin'),
('DL', 'Subaru'),
('E-Class', 'Mercedes-Benz'),
('E63', 'Mercedes-Benz'),
('Elantra', 'Hyundai'),
('EQC', 'Mercedes-Benz'),
('Euro', 'Honda'),
('Excel', 'Hyundai'),
('Fe', 'Hyundai'),
('FF-1', 'Subaru'),
('Fiori', 'Subaru'),
('Forester', 'Subaru'),
('FX', 'Hyundai'),
('G-Class', 'Mercedes-Benz'),
('Genesis', 'Hyundai'),
('Getz', 'Hyundai'),
('GF', 'Subaru'),
('Giulia', 'Alfa Romeo'),
('Giulietta', 'Alfa Romeo'),
('GL', 'Subaru'),
('GL-Class', 'Mercedes-Benz'),
('GLA-Class', 'Mercedes-Benz'),
('GLB-Class', 'Mercedes-Benz'),
('GLC-CLASS', 'Mercedes-Benz'),
('GLC63', 'Mercedes-Benz'),
('GLE-CLASS', 'Mercedes-Benz'),
('GLS-CLASS', 'Mercedes-Benz'),
('Grandeur', 'Hyundai'),
('GT', 'Alfa Romeo'),
('GTV', 'Alfa Romeo'),
('HR-V', 'Honda'),
('i20', 'Hyundai'),
('i30', 'Hyundai'),
('i40', 'Hyundai'),
('i45', 'Hyundai'),
('iLOAD', 'Hyundai'),
('iMAX', 'Hyundai'),
('Impreza', 'Subaru'),
('Insight', 'Honda'),
('Integra', 'Honda'),
('Ioniq', 'Hyundai'),
('iX35', 'Hyundai'),
('Jazz', 'Honda'),
('Journey', 'Dodge'),
('Kona', 'Hyundai'),
('Lagonda', 'Aston Martin'),
('Lantra', 'Hyundai'),
('Lavita', 'Hyundai'),
('Legend', 'Honda'),
('Leone', 'Subaru'),
('Levorg', 'Subaru'),
('Liberty', 'Subaru'),
('Life', 'Honda'),
('M-Class', 'Mercedes-Benz'),
('MARCO', 'Mercedes-Benz'),
('MB100', 'Mercedes-Benz'),
('MB140', 'Mercedes-Benz'),
('MDX', 'Honda'),
('Mito', 'Alfa Romeo'),
('Montreal', 'Alfa Romeo'),
('N360', 'Honda'),
('Nexo', 'Hyundai'),
('Nitro', 'Dodge'),
('NSX', 'Honda'),
('Odyssey', 'Honda'),
('Outback', 'Subaru'),
('Palisade', 'Hyundai'),
('Phoenix', 'Dodge'),
('POLO', 'Mercedes-Benz'),
('Prelude', 'Honda'),
('R-Class', 'Mercedes-Benz'),
('Rapide', 'Aston Martin'),
('RX', 'Subaru'),
('S', 'Hyundai'),
('S-Class', 'Mercedes-Benz'),
('S2000', 'Honda'),
('S600', 'Honda'),
('S65', 'Mercedes-Benz'),
('S800', 'Honda'),
('SANTA', 'Hyundai'),
('SFX', 'Hyundai'),
('Sherpa', 'Subaru'),
('SKI', 'Subaru'),
('SL-Class', 'Mercedes-Benz'),
('SLC-CLASS', 'Mercedes-Benz'),
('SLK-Class', 'Mercedes-Benz'),
('SLS', 'Mercedes-Benz'),
('Sonata', 'Hyundai'),
('Spider', 'Alfa Romeo'),
('SPORTSWAGON', 'Subaru'),
('Sprint', 'Alfa Romeo'),
('Sprinter', 'Mercedes-Benz'),
('Stelvio', 'Alfa Romeo'),
('SUMMER', 'Subaru'),
('SVX', 'Subaru'),
('SX', 'Hyundai'),
('Terracan', 'Hyundai'),
('Tiburon', 'Hyundai'),
('Tonale', 'Alfa Romeo'),
('Trajet', 'Hyundai'),
('Tribeca', 'Subaru'),
('Tucson', 'Hyundai'),
('V-Class', 'Mercedes-Benz'),
('V12', 'Aston Martin'),
('V8', 'Aston Martin'),
('Valente', 'Mercedes-Benz'),
('Vanquish', 'Aston Martin'),
('VANTAGE', 'Aston Martin'),
('Veloster', 'Hyundai'),
('Venue', 'Hyundai'),
('Viano', 'Mercedes-Benz'),
('Virage', 'Aston Martin'),
('Vito', 'Mercedes-Benz'),
('Volante', 'Aston Martin'),
('WAGON', 'Subaru'),
('WRX', 'Subaru'),
('X-CLASS', 'Mercedes-Benz'),
('XT', 'Subaru'),
('XV', 'Subaru'),
('Z', 'Honda');

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

CREATE TABLE `cars` (
  `vin` varchar(17) NOT NULL,
  `model` varchar(255) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `damagetype` varchar(255) DEFAULT NULL,
  `damagearea` varchar(255) DEFAULT NULL,
  `pictures` text DEFAULT NULL,
  `notes` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`vin`, `model`, `year`, `damagetype`, `damagearea`, `pictures`, `notes`) VALUES
(' 25312259', 'Sonata', 2011, 'FRONT & REAR', 'COLLISION', 'resizer.jpg', 'Clean interior.'),
(' 26369435', 'CR-X', 2001, 'WATER', 'MECHANICAL', 'resiadsfasdfasdfzer.jpg', 'Engine gone. Parts good.'),
(' 26383975', 'VANTAGE', 2020, 'FIRE', 'EXTERIOR', 'readsfassizer.jpg', 'Interior clean. Smells nice too.'),
(' 26453435', 'Camry', 2012, 'COLLISION', 'REAR', 'resizder.jpg', 'Front end good.'),
('123456789101213', 'Accord', 2017, 'COLLISION', 'FRONT LEFT', 'readsfasdfasizer.jpg', 'Took off transmition.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carmodels`
--
ALTER TABLE `carmodels`
  ADD PRIMARY KEY (`model`);

--
-- Indexes for table `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`vin`),
  ADD KEY `model` (`model`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cars`
--
ALTER TABLE `cars`
  ADD CONSTRAINT `cars_ibfk_1` FOREIGN KEY (`model`) REFERENCES `carmodels` (`model`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
